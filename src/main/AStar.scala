package main

object AStar {

  def searchTree(start: State): Boolean = {
    var openSet = List[Node]()
    openSet = openSet :+ new Node(start)
    while (!openSet.isEmpty) {
      val current = openSet.min(Ordering.by((x: Node) => x.getFCost))
      if (isGoal(current)) {
        printPath(current)
        return true
      }
      openSet = openSet.filter(x => !x.equals(current))
      current.getState.neighbours.foreach((x: State) => openSet = openSet :+ new Node(x, current))
    }
    return false
  }

  def searchGraph(start: State): Boolean = {
    var fringe = List[Node]()
    var allSet = List[Node]()
    fringe = fringe :+ new Node(start)
    allSet = allSet :+ new Node(start)
    while (!fringe.isEmpty) {
      val current = fringe.min(Ordering.by((x: Node) => x.getFCost))
      if (isGoal(current)) {
        printPath(current)
        return true
      }
      fringe = fringe.filter(x => !x.equals(current))
      current.getState.neighbours.foreach((x: State) => {
        if (!allSet.contains(x)) {
          val add = new Node(x, current)
          fringe = fringe :+ add
          allSet = allSet :+ add
        }
      })
    }
    return false
  }

  private def isGoal(node: Node): Boolean =
    node.getState.manhattanMisplacedValue == 0


  private def printPath(node: Node){
    var path = List[Node]()
    var inode = node
    println(node)
    while (inode != null) {
      path = path :+ inode
      inode = inode.getParent
    }
    path = path.reverse
    path.foreach(x => println(x.getState.print))
    println("Steps: " + (path.size - 1))
  }
}
