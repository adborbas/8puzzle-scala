package main


/**
 * Created by adam on 1/27/14.
 */
case class Position(x: Int, y: Int) {

  def this(oneDimPosition: Int) =
    this(oneDimPosition / 3, oneDimPosition % 3)
}
