package main

/**
 * Created by adam on 1/27/14.
 */
case class Digit(value: Int, position: Position) {

  def this(value: Int) = this(value, new Position(0, 0))

  val requiredPosition: Position =
    new Position(value / 3, value % 3)

  val manhattanMisplacementValue: Int =
    Math.abs(position.x - requiredPosition.x) + Math.abs(position.y - requiredPosition.y)
}
