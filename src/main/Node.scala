package main

class Node(state: State, parent: Node) {
  val cost: Int = if (parent != null) parent.cost + 1 else 0

  val getFCost: Int =
    cost + state.manhattanMisplacedValue

  val getParent: Node = this.parent

  val getState: State = state

  def this(state: State) = this(state, null)

  override def toString: String = "State:" + state.toString + "    Parent: " + parent

  override def equals(obj: scala.Any): Boolean =
    if (!obj.isInstanceOf[Node]) false
    else obj.asInstanceOf[Node].getState == this.getState
}
