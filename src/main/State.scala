package main

import ownjava.RandomHelper

/**
 * Created by adam on 1/27/14.
 */
case class State(digits: Array[Int]) {

  /**
   * Creates a new State from the given String
   * @param digits example: "012345678"
   * @return a new State
   */
  def this(digits: String) = this {
    digits.map(x => Integer.parseInt(x toString)).toArray
  }

  def this() = this(RandomHelper.getRandomSequence: String)


  def print: String = {
    def toStringRec(board: Array[Int], state: Int): String = {
      if (board.isEmpty) ""
      else {
        if (state % 3 == 0)
          "\n\t" + board.head.toString + toStringRec(board.tail, state + 1)
        else "\t" + board.head.toString + toStringRec(board.tail, state + 1)
      }
    }
    toStringRec(digits, 0) + "\n--------------"
  }

  def manhattanMisplacedValue: Int = {
    var index = 0
    var sum = 0
    digits.foreach(x => {
      sum += Math.abs(x - index)
      index += 1
    })
    sum
  }

  def neighbours: List[State] = {
    val iZero = digits.indexOf(0)
    def moveZero(offset: Int): State = {
      val newState = new State(digits.clone())
      val t = newState.digits(iZero + offset)
      newState.digits(iZero) = t
      newState.digits(iZero + offset) = 0
      newState
    }
    var result = List[State]()
    if (iZero > 2) // Up
      result = result :+ moveZero(-3)
    if (iZero < 6) // Down
      result = result :+ moveZero(3)
    if (iZero > 0) // Left
      result = result :+ moveZero(-1)
    if (iZero < 8) // Right
      result = result :+ moveZero(1)
    result
  }
}
