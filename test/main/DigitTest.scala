package main

import org.scalatest.FunSpec

/**
 * Created by adam on 1/27/14.
 */
class DigitTest extends FunSpec {

  describe("\nDigit 0") {

    var digit = new Digit(0, new Position(0, 0))
    it("should have a requiredPosition of 0, 0") {
      assert(digit.requiredPosition.equals(new Position(0, 0)))
    }

    describe("manhattanMisplacementValue") {
      describe("in position 0, 0") {
        it("should be 0") {
          digit = new Digit(0, new Position(0, 0))
          assert(digit.manhattanMisplacementValue == 0)
        }
      }

      describe("in position 0, 1") {
        it("should be 1") {
          digit = new Digit(0, new Position(0, 1))
          assert(digit.manhattanMisplacementValue == 1)
        }
      }

      describe("in position 2, 2") {
        it("should be 4") {
          digit = new Digit(0, new Position(2, 2))
          assert(digit.manhattanMisplacementValue == 4)
        }
      }
    }
  }

  describe("\nDigit 3") {

    var digit = new Digit(3, new Position(0, 0))
    it("should have a requiredPosition of 1, 0") {
      assert(digit.requiredPosition.equals(new Position(1, 0)))
    }

    describe("manhattanMisplacementValue") {

      describe("in position 1, 0") {
        it("should be 0") {
          digit = new Digit(3, new Position(1, 0))
          assert(digit.manhattanMisplacementValue == 0)
        }
      }

      describe("in position 0, 0") {
        it("should be 1") {
          digit = new Digit(3, new Position(0, 0))
          assert(digit.manhattanMisplacementValue == 1)
        }
      }

      describe("in position 0, 1") {
        it("should be 2") {
          digit = new Digit(3, new Position(0, 1))
          assert(digit.manhattanMisplacementValue == 2)
        }
      }

      describe("in position 2, 2") {
        it("should be 3") {
          digit = new Digit(3, new Position(2, 2))
          assert(digit.manhattanMisplacementValue == 3)
        }
      }
    }
  }
}
