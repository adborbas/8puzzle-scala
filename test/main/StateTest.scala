package main

import org.scalatest.FunSpec

/**
 * Created by adam on 1/27/14.
 */
class StateTest extends FunSpec {

  describe("Goal state") {
    val state = new State("012345678")
    it("should have a manhattanMisplacedValue of 0 ") {
      assert(state.manhattanMisplacedValue == 0)
    }
    it("should have 2 neighbours") {
      assert(state.neighbours.length == 2)
    }
  }

  describe("102345678 state") {
    val state = new State("102345678")
    it("should have a manhattanMisplacedValue of 2 ") {
      assert(state.manhattanMisplacedValue == 2)
    }
    it("should have 3 neighbours") {
      assert(state.neighbours.length == 3)
    }
  }

  describe("570138642 state") {
    val state = new State("570138642")
    it("should have a manhattanMisplacedValue of 14 ") {
      assert(state.manhattanMisplacedValue == 14)
    }
    it("should have 2 neighbours") {
      assert(state.neighbours.length == 2)
    }
  }

  describe("123608754 state") {
    val state = new State("123608754")
    it("should have a manhattanMisplacedValue of 14 ") {
      assert(state.manhattanMisplacedValue == 14)
    }
    it("should have 4 neighbours") {
      assert(state.neighbours.length == 4)
    }
  }

  describe("142305678 state") {
    val state = new State("142305678")
    it("should have a manhattanMisplacedValue of 4 ") {
      assert(state.manhattanMisplacedValue == 4)
    }
    it("should have 4 neighbours") {
      assert(state.neighbours.length == 4)
    }
  }

}
